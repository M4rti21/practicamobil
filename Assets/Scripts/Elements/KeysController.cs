using UnityEngine;
using UnityEngine.InputSystem;

public class KeysController : MonoBehaviour
{
    [SerializeField]
    private KeyController[] m_Keys;

    [SerializeField]
    private InputActionAsset p_InputAsset;
    public static InputActionAsset p_Input;

    private void Awake()
    {
        p_Input = Instantiate(p_InputAsset);
    }

    private void Start()
    {
        Subscribe();
    }

    public void Subscribe()
    {
        foreach (KeyController key in m_Keys)
        {
            p_Input.FindActionMap("gameplay").FindAction(key.NoteID).started += key.ClickStart;
            p_Input.FindActionMap("gameplay").FindAction(key.NoteID).canceled += key.ClickEnd;
        }
        p_Input.FindActionMap("gameplay").Enable();
    }

    public void Unsubscribe()
    {
        foreach (KeyController key in m_Keys)
        {
            p_Input.FindActionMap("gameplay").FindAction(key.NoteID).started -= key.ClickStart;
            p_Input.FindActionMap("gameplay").FindAction(key.NoteID).canceled -= key.ClickEnd;
        }
        p_Input.FindActionMap("gameplay").Disable();
    }

}
