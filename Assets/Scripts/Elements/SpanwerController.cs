using System.Collections;
using UnityEngine;

public class SpanwerController : MonoBehaviour
{
    public GameObject Note;
    public float Rate;
    public float StartSpeed;
    public static float Speed;
    public float[] NoteRails;
    private void Start()
    {
        Speed = StartSpeed;
        StartCoroutine(spawnNote());
        StartCoroutine(countSeconds());
    }
    IEnumerator spawnNote()
    {
        while (true)
        {
            GameObject newNote = Instantiate(Note);
            int position = Random.Range(0, NoteRails.Length);
            newNote.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2 (0, -Speed);
            newNote.gameObject.GetComponent<Rigidbody2D>().transform.position = new Vector2(NoteRails[position]-1f, 8);
            yield return new WaitForSeconds(Rate);
        }
    }
    IEnumerator countSeconds()
    {
        while (true)
        {
            yield return new WaitForSeconds(4);
            Speed += 0.1f;
            if (Rate > 0.01)
            {
                Rate -= 0.01f;
            }
        }
    }
}
