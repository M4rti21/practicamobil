using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.InputSystem.Utilities;

public class NoteController : MonoBehaviour
{
    public void GetClicked(int score)
    {
        StatsScript.Instance.modifyScore(score);
        StatsScript.Instance.modifyNotecount();
        Destroy(this.gameObject);
    }

}
