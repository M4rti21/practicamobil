using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class KeyController : MonoBehaviour
{
    [SerializeField]
    private string m_NoteID;
    public string NoteID => m_NoteID;

    public OffsetController Offset;
    private List<NoteController> Notes;

    public AudioSource audioData;

    private void Awake()
    {
        Notes = new List<NoteController>();
    }

    public void ClickStart(InputAction.CallbackContext context)
    {
        audioData.Play();
        this.GetComponent<ParticleSystem>().Play();
        gameObject.transform.localScale = new Vector3(0.9f, 0.9f, 0.01f);
        if (Notes.Count < 1)
        {
            Offset.SetText("MISS");
            StatsScript.Instance.modifyHP(-10);
            this.GetComponent<Animator>().Play(0);
        }
        else
        {
            int o = OverlapCalc(Notes[0].GetComponent<CircleCollider2D>());
            Notes[0]?.GetClicked(o);
            switch (o)
            {
                case 0:
                    Offset.SetText("MISS");
                    StatsScript.Instance.modifyHP(-10);
                    this.GetComponent<Animator>().Play(0);
                    break;
                case 50:
                    Offset.SetText("MEH");
                    StatsScript.Instance.modifyHP(0);
                    break;
                case 100:
                    Offset.SetText("GOOD");
                    StatsScript.Instance.modifyHP(2);
                    break;
                case 200:
                    Offset.SetText("GREAT");
                    StatsScript.Instance.modifyHP(5);
                    break;
                case 300:
                    Offset.SetText("PERFECT");
                    StatsScript.Instance.modifyHP(10);
                    break;
                default:
                    Offset.SetText("MISS");
                    StatsScript.Instance.modifyHP(-10);
                    break;
            }
            
        }
        
    }

    private int OverlapCalc(CircleCollider2D c)
    {
        Vector2 n = c.transform.position;
        Vector2 k = this.GetComponent<CircleCollider2D>().transform.position;

        float r = c.radius;

        float d = Vector2.Distance(n, k);

        if (d > (r * 2)) return 0;
        if (d > (r * 1)) return 50;
        if (d > (r * .5)) return 100;
        if (d > (r * .15)) return 200;
        else return 300;

    }
    public void ClickEnd(InputAction.CallbackContext context)
    {
        this.gameObject.transform.localScale = new Vector3(1f, 1f, 0.01f);
        this.GetComponent<ParticleSystem>().Stop();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Note"))
        {
            Notes.Add(collision.gameObject.GetComponent<NoteController>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Note"))
        {
            Notes.Remove(collision.gameObject.GetComponent<NoteController>());
        }
    }

}
