using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        StatsScript.Instance.modifyHP(-10);
        StatsScript.Instance.modifyNotecount();
        StatsScript.Instance.modifyScore(0);
        Destroy(collision.gameObject);
    }
}
