using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class StatsScript : MonoBehaviour
{
    [SerializeField]
    private static StatsScript m_Instance;
    public static StatsScript Instance => m_Instance;

    public GameEvent onGameStart;
    public GameEvent onGameEnd;

    public GameEvent scoreChange;
    public GameEvent accChange;
    public GameEvent comboChange;
    public GameEvent hpChange;

    public int hp;
    public int combo;
    public int maxCombo;
    public int score;
    public int rawScore;
    public int acc;
    public int noteCount;

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        StartGame();
    }
    private void ResetStats()
    {
        hp = 100;
        combo = 0;
        maxCombo = 0;
        score = 0;
        rawScore = 0;
        acc = 0;
        noteCount = 0;
    }

    public void modifyNotecount()
    {
        noteCount ++;
        modifyAcc();
    }
    public void modifyHP(int n)
    {
        hp += n;
        if (hp > 100) hp = 100;
        if (hp <= 0) EndGame();
        hpChange.Raise();
    }
    public void modifyScore(int n)
    {
        if (n > 0) combo += 1;
        else combo = 0;
        if (combo > maxCombo) maxCombo = combo;
        score += n * combo;
        rawScore += n;
        scoreChange.Raise();
        comboChange.Raise();
    }

    public void modifyAcc()
    {
        float maxScore = noteCount * 300f;
        acc = (int) (rawScore / maxScore * 100f);
        accChange.Raise();
    }
    public void StartGame()
    {
        ResetStats();
    }

    public void EndGame()
    {
        onGameEnd.Raise();
        SceneManager.LoadScene("result");
    }

}
