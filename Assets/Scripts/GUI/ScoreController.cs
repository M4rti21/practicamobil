using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI m_TextScore;
    [SerializeField]
    private TextMeshProUGUI m_TextCombo;
    [SerializeField]
    private TextMeshProUGUI m_TextAcc;

    private void Start()
    {
        SetScore();
        SetAcc();
        SetCombo();
    }

    public void SetScore()
    {
        m_TextScore.text = StatsScript.Instance.score.ToString();
    }

    public void SetAcc()
    {
        m_TextAcc.text = StatsScript.Instance.acc.ToString();
    }

    public void SetCombo()
    {
        m_TextCombo.text =  StatsScript.Instance.maxCombo.ToString();
    }
}
