using TMPro;
using UnityEngine;

public class ResultScore : MonoBehaviour
{
    void Start()
    {
        this.GetComponent<TextMeshProUGUI>().text = "Score: " + StatsScript.Instance.score;
    }
}
