using System.Collections;
using TMPro;
using UnityEngine;

public class OffsetController : MonoBehaviour
{
    TextMeshProUGUI tmp;
    private void Start()
    {
        tmp = this.GetComponent<TextMeshProUGUI>();
    }
    public void SetText(string txt)
    {
        StopAllCoroutines();
        tmp.text = txt;

        if (txt.Equals("MISS")) tmp.color = new Color(255, 0, 0, 1);
        if (txt.Equals("MEH")) tmp.color = new Color(255, 255, 0, 1);
        if (txt.Equals("GOOD")) tmp.color = new Color(0, 255, 0, 1);
        if (txt.Equals("GREAT")) tmp.color = new Color(0, 255, 255, 1);
        if (txt.Equals("PERFECT")) tmp.color = new Color(200, 200, 200, 1);
        
        StartCoroutine(clear());
    }
    IEnumerator clear()
    {
        yield return new WaitForSeconds(0.4f);
        tmp.text = "";
    }
}
