using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpBarController : MonoBehaviour
{
    public void ChangeHp()
    {
        GetComponent<Image>().fillAmount = StatsScript.Instance.hp / 100f;
    }
}
